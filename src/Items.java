
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Items {
 
	private String sku;
	private String options;
	private int quantity;
	private String found;
	public Items(String sku, String options, int quantity, String found)
	{	
		this.sku = sku;
		this.options = options;
		this.quantity = quantity;
		this.found = found;
	}
	public String getSku()
	{
		return sku;
	}
	public String getOptions()
	{
		return options;
	}
	public int getQuantity()
	{
		return quantity;
	}
	public String getFound()
	{
		return found;
	}
	public void setFound(String found)
	{
		this.found = found;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public void setOptions(String options)
	{
		this.options = options;
	}
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	public String toString() {
	    return sku + " : " + options + " : " + quantity + " : " + found;
	}
	
}
