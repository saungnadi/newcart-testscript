
//Sandi Lwin
//New Cart Test Script on Build 2016.1r14 using Selenium_2.47 on Firefox 38.0. Generates custom log file with test results. TestNG_6.8 report file contains log events.
//Automated Build setup on Jenkins_1.643

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import org.apache.commons.io.FileUtils;
import org.testng.Reporter;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import org.apache.log4j.*;

public class NewCart_Clean {
		
		public static WebDriver driver;		
	    public static void main(String[] args) throws Exception					
		{
			
			String[] pidArray = {"1137896","1269537","1458762","1512629","1154035"};
			String pid = "";
	    	int optionIdx = 0;
	  		String option = "";
	  		int quantity = 0;
	  		String found = "False";
	  		int tempQty = 0;
	  		int maxQty = 10;
	  		int qtyCount = 0;
	  		boolean stretchpay = false;
	  		int count = 0;
	  		String filename = new SimpleDateFormat("yyyyMMddhhmm'.txt'").format(new Date());
	  		PrintWriter printWriter = new PrintWriter ("C:\\PlaceOrders"+ filename);
	  		int itemsIncart = 1;											
	  		int numofOrderstoExe = 3; 
	  		 		
	  		for (int y =0; y < numofOrderstoExe ;y++)
	  		{
	  			driver = new FirefoxDriver();
	  			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	  	    	ArrayList<Items> item_list = new ArrayList<Items>();
	  	    	
		    	for( int x = 0; x <itemsIncart; x++)
		    	{
		    		
		      		int idx = new Random().nextInt(pidArray.length);
		      		pid = (pidArray[idx]); 
			  		driver.navigate().to("https://development.web.mcg.demandware.net/on/demandware.store/Sites-jtv-Site/default/product-Show?pid=" + pid);
				    String item_description = "";
				    String item_sku = "";
				    item_description = driver.findElement(By.xpath("//*[@id='title-row']/h1")).getText();
				    item_sku = driver.findElement(By.id("sku-row")).getText();
				    int itemcount = x + 1;
				   
				    Reporter.log("Item " + itemcount + " description: " + item_description);
				    printWriter.println("Item " + itemcount + " description: " + item_description);
				    
				    Reporter.log("Product id:" + pid + " sku: " + item_sku);
				    printWriter.println("Product id:" + pid + " sku: " + item_sku);
		
				    boolean size = isElementPresent("sizeOption"); 
				    if (size == true)
				    {
				    	String[] sizeArray;
				    	Integer sizeIdx = null;
				    	Select selectedSz = new Select(driver.findElement(By.id("sizeOption")));
				    	List<WebElement> sizeOptions = selectedSz.getOptions();
				    	sizeArray = new String[sizeOptions.size()-1];
				    	for(int s = 1; s < sizeOptions.size(); s++)		
				    	{	
				    		sizeArray[s-1] = Integer.toString(s);
				    		int tempSizeIdx = Integer.parseInt(sizeArray[s-1]);
				    		Reporter.log("Sizes: "  + sizeOptions.get(tempSizeIdx).getText());
				    	}
			      		sizeIdx = new Random().nextInt(sizeArray.length);
			      		int tempIdx = Integer.parseInt(sizeArray[sizeIdx]);
			      		printWriter.println("Selected size: "  + sizeOptions.get(tempIdx).getText());
				    	selectedSz.selectByIndex(tempIdx);
				    }
				    boolean qty = isElementPresent("qtyOption");
				    if (qty == true)
				    {
				    	String[] qtyArray;
					    Integer qtyIdx = null;
				    	Select selectedQty = new Select(driver.findElement(By.id("qtyOption")));
				    	List<WebElement> qtyOptions = selectedQty.getOptions();
				    	qtyArray = new String[qtyOptions.size()];
				    	for(int s = 0; s < qtyOptions.size(); s++)			
				    	{	
				    		qtyArray[s] = qtyOptions.get(s).getText();
				    		Reporter.log("Qty: "  + qtyArray[s]);
				    	}
				    	qtyIdx = new Random().nextInt(qtyArray.length);
			      		Reporter.log("Qty index "  + qtyIdx);
			      		quantity = Integer.parseInt(qtyOptions.get(qtyIdx).getText());
			      		qtyCount += quantity;
			      		Reporter.log("Qty selected: "  + qtyOptions.get(qtyIdx).getText());
			      		printWriter.println("Qty selected: "  + qtyOptions.get(qtyIdx).getText());
			      		Reporter.log("Qty count: "  + qtyCount);
			      		printWriter.println("Qty count: "  + qtyCount);
				    	selectedQty.selectByIndex(qtyIdx);
				    }
				    String prdBitmask = "";
				    String appraisal = null;
				    String warranty = null;
				    String stretch = null;
				    stretch = "0";
				    
				    if (isElementPresent("input_option_appraisal")){appraisal = "1";}
				    else {appraisal = "0";}
				    if (isElementPresent("input_option_warranty")){warranty = "1";}
				    else {warranty = "0";}
				   
				    prdBitmask = appraisal+warranty+stretch;
				    if ( !prdBitmask.equals("000"))
				    {
				    	if(prdBitmask.equals("111"))		
				    	{
				    		String[] set1Array ={"000","010","110","100","001","011","101","111"};
				      		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s) " + option);
				      		if (option.equals("001"))
			      			{
			      				driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("stretchpay");
			      				printWriter.println("stretchpay");	
			      				stretchpay = true;
			      			}
			      			if (option.equals("010"))
			      			{
		      					driver.findElement(By.id("input_option_warranty")).click();
		      					Reporter.log("warranty");
		      					printWriter.println("warranty");
			      			}
			      			if (option.equals("011"))
			      			{
		      					driver.findElement(By.id("input_option_warranty")).click();
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("warranty and stretchpay");
			      				printWriter.println("warranty and stretchpay");
			      				stretchpay = true;
			      			}
			      			if (option.equals("100"))
			      			{
			      					driver.findElement(By.id("input_option_appraisal")).click();
			      					Reporter.log("appraisal");
			      					printWriter.println("appraisal");	
			      			}
			      			if (option.equals("101"))
			      			{
			      				
		      					driver.findElement(By.id("input_option_appraisal")).click();
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("appraisal and stretchpay");
			      				printWriter.println("appraisal and stretchpay");
			      				stretchpay = true;
			      			}
			      			if (option.equals("110"))
			      			{
		      					driver.findElement(By.id("input_option_appraisal")).click();
		      					driver.findElement(By.id("input_option_warranty")).click();
			      				Reporter.log("appraisal and warranty");
			      				printWriter.println("appraisal and warranty");
			      			}
			      			if (option.equals("111"))
			      			{
			      				driver.findElement(By.id("input_option_appraisal")).click();
		      					driver.findElement(By.id("input_option_warranty")).click();
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("appraisal, warranty and stretchpay");
			      				printWriter.println("appraisal, warranty and stretchpay");
			      				stretchpay = true;
			      			}	
				    	}
				    	
				    	if(prdBitmask.equals("100"))		
				    	{
				    		String[] set1Array ={"000","100"}; 
				    		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s)" + option);
			      			if (option.equals("100"))
			      			{
			      				driver.findElement(By.id("input_option_appraisal")).click();
			      				Reporter.log("appraisal");
			      				printWriter.println("appraisal");
			      			}
				    	}
				    	
				    	if(prdBitmask.equals("010"))		
				    	{
				      		String[] set1Array ={"000","010"};
				      		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s) " + option);
			      			if (option.equals("010"))
			      			{
		      					driver.findElement(By.id("input_option_warranty")).click();
			      				Reporter.log("warranty");
			      				printWriter.println("warranty");
			      			}
				    	}	
				    	
				    	if(prdBitmask.equals("001"))		
				    	{
				      		String[] set1Array ={"000","001"};
				      		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s) " + option);
			      			if (option.equals("001"))
			      			{
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("stretchpay");
			      				printWriter.println("stretchpay");
			      				stretchpay = true;
			      			}
				    	}
				    	
				    	if(prdBitmask.equals("110"))		
				    	{
				      		String[] set1Array ={"000","100","010","110"};
				      		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s) " + option);
			      			if (option.equals("100"))
			      			{
			      				driver.findElement(By.id("input_option_appraisal")).click();
			      				Reporter.log("appraisal");
			      				printWriter.println("appraisal");
			      			}
			      			if (option.equals("010"))
			      			{
		      					driver.findElement(By.id("input_option_warranty")).click();
			      				Reporter.log("Selected warranty");
			      				printWriter.println("Selected warranty");
			      			}
			      			if (option.equals("110"))
			      			{
			      				driver.findElement(By.id("input_option_appraisal")).click();
		      					driver.findElement(By.id("input_option_warranty")).click();
			      				Reporter.log("appraisal and warranty");
			      				printWriter.println("appraisal and warranty");
			      			}
				    	}
				    	
				    	if(prdBitmask.equals("101"))		
				    	{
				      		String[] set1Array ={"000","001","100","101"};
				      		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s) " + option); 
			      			if (option.equals("100"))
			      			{
			      				driver.findElement(By.id("input_option_appraisal")).click();
			      				Reporter.log("appraisal");
			      				printWriter.println("appraisal"); 
			      			}
			      			if (option.equals("001"))
			      			{
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("stretchpay");
			      				printWriter.println("stretchpay"); 
			      				stretchpay = true;
			      			}
			      			if (option.equals("101"))
			      			{
			      				driver.findElement(By.id("input_option_appraisal")).click();
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("appraisal and stretchpay");
			      				printWriter.println("appraisal and stretchpay");
			      				stretchpay = true;
			      			}
				    	}
				    	if(prdBitmask.equals("011"))		
				    	{
				      		String[] set1Array ={"000","001","010","011"};
				      		optionIdx = new Random().nextInt(set1Array.length);
				      		option = (set1Array[optionIdx]); 
				      		Reporter.log("Product option(s) " + option);
				      		printWriter.println("Product option(s) " + option);
			      			if (option.equals("001"))
			      			{
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("stretchpay");
			      				printWriter.println("stretchpay");
			      				stretchpay = true;
			      			}
			      			if (option.equals("010"))
			      			{
		      					driver.findElement(By.id("input_option_warranty")).click();
			      				Reporter.log("warranty");
			      				printWriter.println("warranty");
			      			}
			      			if (option.equals("011"))
			      			{
		      					driver.findElement(By.id("input_option_warranty")).click();
		      					driver.findElement(By.id("stretchpay_checkbox")).click();
			      				Reporter.log("warranty and stretchpay");
			      				printWriter.println("warranty and stretchpay");
			      				stretchpay = true;
			      			}
				    	}
				    	Thread.sleep(5000);
				    }
				    
				    boolean duplicate = false;
				    if (isElementPresent("add-to-cart-button"))
				    {
				    	driver.findElement(By.id("add-to-cart-button")).click();
					    Thread.sleep(5000); 
					    
					    if ( item_list.size() == 0)
					    {	
					    	item_list.add(new Items (item_sku, option, quantity, found));
					    	Items i = item_list.get(0);
					    	Reporter.log("Inserting: "+ i);
					    }
						else
						{	
						    for (int m = 0; m <item_list.size(); m++)
						    {
						    	if (item_list.get(m).getSku().equals(item_sku) )
							    {	
						    		tempQty = item_list.get(m).getQuantity();
							    	tempQty += quantity;
							    	item_list.get(m).setQuantity(tempQty);
								    Reporter.log("Update quantity in interation " + m +" to: " + item_list.get(m).getQuantity());
								    duplicate = true;
							    }
						    	else
						    	{
						    		if ( m == (item_list.size() -1) && duplicate == false )
								    {	
						    			item_list.add(new Items (item_sku, option, quantity, found));			
								    	Reporter.log("Not a duplicate, added: " + item_list.get(m+1).getSku() + ", " +item_list.get(m+1).getOptions()+ ", "+item_list.get(m+1).getQuantity());
								    	break;
								    }
						    	}
						    }   
						}
				    }
		    	}
		    	for(int i = 0; i < item_list.size(); i++){
		    		String temp = item_list.get(i).toString();
		    	    Reporter.log(temp);
		    	    printWriter.println("Items before cart verification:");
		    	    printWriter.println(item_list.get(i));
		    	}	
			    driver.navigate().to("https://development.web.mcg.demandware.net/on/demandware.store/Sites-jtv-Site/default/cart-Show");
			   	
		    	if (qtyCount <= maxQty)
		    	{	
				    String[] checkoutArray ={};
				    if (stretchpay == true)
			  		{
			  			checkoutArray = new String[]{"//*[@id='totals']/div[2]/form/button[1]"};				
			  		}
			  		else
			  		{	
			  			checkoutArray = new String[]{"//*[@id='totals']/div[2]/form/button[1]",					
													 "//*[@id='totals']/div[2]/form/button[2]"};			    
			  		}
			  		
			  		int coIdx = new Random().nextInt(checkoutArray.length);
			  		String chkoutXpath = checkoutArray[coIdx]; 
			  		String chkoutMethod = convertToName(chkoutXpath);
			  		String user = null;
			  		Random random = new Random();
		  			int randnum = random.nextInt(1000 - 0 + 1) + 0;
		  			Thread.sleep(5000);
		  			
	  	    		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	  	    		FileUtils.copyFile(scrFile, new File("C:\\" +"Order" + count +"Cart_screenshot" +"-" + filename + ".png"));
			  		
			  		Reporter.log("Checkout Method: " + chkoutMethod);
			  		printWriter.println("Checkout Method: " + chkoutMethod);
			  		if(chkoutMethod == "Regular Checkout" && isElementPresent(chkoutXpath))
			  		{
			  			driver.findElement(By.xpath(chkoutXpath)).click();
			  			Thread.sleep(5000);
			  			String[] userArray ={"JTV"/*,"Guest","NewAcct"*/};
			      		int userIdx = new Random().nextInt(userArray.length);
			      		user = (userArray[userIdx]); 

			  			if (user == "JTV")
					    {
			  				if (isElementPresent("qv_add_button"))
			  				{
				  				driver.findElement(By.name("dwfrm_login_username")).sendKeys("chris@merchant.com");
						        driver.findElement(By.name("dwfrm_login_password")).sendKeys("password");
						        driver.findElement(By.id("qv_add_button")).click();
						        Reporter.log("JTV user");
						  		printWriter.println("JTV user");
			  				}
					    }
			  			if (user == "Guest")
					    {
			  				if (isElementPresent("guest_button"))
			  				{
				  				driver.findElement(By.id("guest_button")).click();
						        Reporter.log("Guest user");
						  		printWriter.println("Guest user");
			  				}
					    }
			  			/*
			  			if(isElementPresent("create_acct_button"))
			  			{	
			  			}
			  			*/
			  		}
			  		if(chkoutMethod == "QuickBuy" && isElementPresent(chkoutXpath) && isElementPresent("//*[@id='totals']/div[3]/form/div[1]/input") && isElementPresent("//*[@id='totals']/div[3]/form/div[2]/input"))
			  		{
			  			driver.findElement(By.xpath("//*[@id='totals']/div[3]/form/div[1]/input")).sendKeys("chris@merchant.com");
				        driver.findElement(By.xpath("//*[@id='totals']/div[3]/form/div[2]/input")).sendKeys("password");
				        driver.findElement(By.xpath(chkoutXpath)).click();
				        user = "JTV";
			  		} 
			  		if(chkoutMethod == "PayPal" && isElementPresent(chkoutXpath))
			  		{
			  			driver.findElement(By.xpath(chkoutXpath)).click(); 
			  			Thread.sleep(5000);

			  			if (isElementPresent("//form[@id='loginForm']/div/div/input")
				  				&& isElementPresent("//form[@id='loginForm']/div[1]/div[2]/input")          
				  				&& isElementPresent("//form[@id='loginForm']/div[2]/input"))               
			  			{
		  					driver.findElement(By.xpath("//form[@id='loginForm']/div/div/input")).sendKeys("tester_sl_1@gmail.com");
		  					driver.findElement(By.xpath("//form[@id='loginForm']/div[1]/div[2]/input")).sendKeys("password123!");
		  					driver.findElement(By.xpath("//form[@id='loginForm']/div[2]/input")).click(); 
			  			}
			  			Thread.sleep(5000);
			  			driver.findElement(By.xpath("//form[@id='ryiForm']/div[1]/input")).click();
			  			Thread.sleep(5000);	
			  		}
					if (user == "JTV")
					{
						if (isElementPresent("//*[@id='dwfrm_singleshipping_addressList']/option[2]"))
						{
							driver.findElement(By.xpath("//*[@id='dwfrm_singleshipping_addressList']/option[2]")).click();		
							Thread.sleep(5000);
						}
					}
					if (user == "Guest")
					{
						if (isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_firstName") 
						&& isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_lastName") && isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_address1")
						&& isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_address2") && isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_city")
						&& isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_country")  && isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_states_state")
						&& isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_zip") 	   && isElementPresent("dwfrm_singleshipping_shippingAddress_addressFields_phone")
						&& isElementPresent("dwfrm_singleshipping_shippingAddress_useAsBillingAddress"))
						{
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_firstName")).sendKeys("test");			
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_lastName")).sendKeys("rand"+ randnum);			
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_address1")).sendKeys("123 Some Street");			
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_address2")).sendKeys("Apt 123");			
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_city")).sendKeys("Johnson City");				
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_country")).sendKeys("U");			
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_states_state")).sendKeys("T");		
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_zip")).sendKeys("37601");				
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_addressFields_phone")).sendKeys("1234567894");				
							driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_useAsBillingAddress")).click();				
							Thread.sleep(10000);
						}
					}
					String[] shipArray ={ "shipping-method-Standard", 	
										  "shipping-method-Expedited",						
										  "shipping-method-Express",						
					  					  "shipping-method-Overnight"};  	
			  		int shipIdx = new Random().nextInt(shipArray.length);
			  		String shipMethod = shipArray[shipIdx]; 
			  		Reporter.log("Shipping Method: " + shipMethod);
			  		printWriter.println("Shipping Method: " + shipMethod);
			  		if (isElementPresent(shipMethod))
			  		{
			  			driver.findElement(By.id(shipMethod)).click();					 
			  		}
			  		if (isElementPresent("dwfrm_singleshipping_shippingAddress_useAsBillingAddress")&& isElementPresent("//*[@id='totals']/div[2]/button"))
			  		{
			  	  		if (!driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_useAsBillingAddress")).isSelected())								  
			  	  		{								 
			  	  			driver.findElement(By.id("dwfrm_singleshipping_shippingAddress_useAsBillingAddress")).click();
			  	  		}
			  	  		driver.findElement(By.xpath("//*[@id='totals']/div[2]/button")).click();			 
			  	  		Thread.sleep(8000);
			  		}	
			  		String totDue = driver.findElement(By.id("totalDue")).getText();
			  		String result = totDue.replaceAll("[$,]","");
			  		double dTot = Double.parseDouble(result);
			  		int TotalDue = (int) dTot;
			  		if(TotalDue >= 24999)
			  		{
			  			isElementPresent("is_CALL_US");
			  		}
			  		else
			  		{
				  		if (chkoutMethod != "PayPal" )
				  		{
					  		String[] billArray ={};
					  		if (stretchpay == true)
					  		{
					  			billArray = new String[]{"is-JTV_PA","is-CREDIT_CARD"};
					  		}
					  		else
					  		{	
					  			billArray = new String[]{"is-BML","is-CREDIT_CARD",
					  									 "is-PAYPAL","is-JTV_PA"};
					  		}
					  		int billIdx = new Random().nextInt(billArray.length);
					  		String billing = (billArray[billIdx]); 
					  		if (isElementPresent(billing))
					  		{
					  			driver.findElement(By.id(billing)).click();
					  			Reporter.log("Billing Method: " + billing);
					  			printWriter.println("Billing Method: " + billing);
					  		}
					  		if(billing == "is-CREDIT_CARD")
					  		{	
					  			if (isElementPresent("dwfrm_billing_paymentMethods_creditCard_owner") && isElementPresent("dwfrm_billing_paymentMethods_creditCard_number") 
					  				&& isElementPresent("dwfrm_billing_paymentMethods_creditCard_month") && isElementPresent("dwfrm_billing_paymentMethods_creditCard_year")
					  				&& isElementPresent("dwfrm_billing_paymentMethods_creditCard_cvn") && isElementPresent("dwfrm_billing_paymentMethods_creditCard_saveCard")
					  				&& isElementPresent("//*[@id='totals']/div[2]/button"))
					  			{
					  	  			if (driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).getAttribute("value").isEmpty())
					  	  			{
					  	  				Thread.sleep(3000);
					  	  				driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_owner")).sendKeys("Chris Testerman");
					  	  				Thread.sleep(3000);
						  				driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_number")).sendKeys("4111111111111111");
						  				Select cc_month = new Select(driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_month"))); 
						  				cc_month.selectByIndex(1);
						  				Thread.sleep(3000);
						  				Select cc_year = new Select(driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_year")));
						  				cc_year.selectByIndex(7);
						  				Thread.sleep(3000);
						  				driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_cvn")).sendKeys("111");
						  	  	  		if (!driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_saveCard")).isSelected())
						  	  	  		{								 
						  	  	  			driver.findElement(By.id("dwfrm_billing_paymentMethods_creditCard_saveCard")).click();
						  	  	  		}
					  	  			}
					  	  			Thread.sleep(3000);
					  	  			driver.findElement(By.xpath("//*[@id='totals']/div[2]/button")).click(); 					
					  	  			Thread.sleep(5000);
					  			}
					  		}
					  		if(billing == "is-PAYPAL")
					  		{
					  			if (isElementPresent("//*[@id='totals']/div[2]/button"))
					  	  		{
					  				driver.findElement(By.xpath("//*[@id='totals']/div[2]/button")).click();   
					  				Thread.sleep(5000);
					  	  		}
					  			
					  			if (isElementPresent("injectedUnifiedLogin"))
					  	  		{
					  				driver.switchTo().frame(0);
					  	  		}
					  			
					  			if (isElementPresent("email")
						  				&& isElementPresent("password")          
						  				&& isElementPresent("btnLogin"))               
					  			{
					  				driver.findElement(By.xpath("//form[@id='loginForm']/div/div/input")).sendKeys("sanditest+1@gmail.com");
				  					driver.findElement(By.xpath("//form[@id='loginForm']/div[1]/div[2]/input")).sendKeys("passwd123!");
				  					driver.findElement(By.xpath("//form[@id='loginForm']/div[2]/input")).click(); 
					  			}
					  			if (isElementPresent("confirmButtonTop"))
					  			{
					  				driver.findElement(By.id("confirmButtonTop")).click();
					  			}
					  		}
					  		if(billing == "is-BML")
					  		{
					  			if (isElementPresent("//*[@id='totals']/div[2]/button"))
					  	  		{
					  				driver.findElement(By.xpath("//*[@id='totals']/div[2]/button")).click();   
					  				Thread.sleep(5000);
					  	  		}
					  		}
					  		if(billing == "is-JTV_PA")
					  		{
					  			Thread.sleep(5000);
					  			if (isElementPresent("//*[@id='totals']/div[2]/button"))
					  	  		{
					  				driver.findElement(By.xpath("//*[@id='totals']/div[2]/button")).click();   
					  				Thread.sleep(5000);
					  	  		}
					  		}
				  		}
				    	for(int i = 0; i < item_list.size(); i++){
				    		item_list.get(i).setFound("False");
				    		String temp = item_list.get(i).toString();
				    	    Reporter.log(temp);
				    	    printWriter.println("item_list before confirmation page:");
				    	    printWriter.println(item_list.get(i));
				    	}	
				    	/* NEED TO ADD THIS BACK!
				    	 * Printing items in cart before payment information
				    	 */
					    for(int z=0; z <item_list.size(); z++)
					    {
					    	String temp = item_list.get(z).getSku();
					    	Reporter.log("Looking for item " + temp);
					    	
					    	for(int s=0; s <itemsIncart; s++)
					    	{
					    		Reporter.log ("Item " + s + " is:" + driver.findElement(By.xpath("//*[@id='cart-item-"+ s +"'"+"]/div/div[2]/div/div[1]/div[1]/div[2]/span")).getText());
					    		if (driver.findElement(By.xpath("//*[@id='cart-item-"+ s +"'"+"]/div/div[2]/div/div[1]/div[1]/div[2]/span")).getText().contains(temp))
						    	{
						    		Reporter.log("Item: " + item_list.get(z)+ " is a match");
						    		item_list.get(z).setFound("True");
						    	}  
						    	else 
						    	{
						    		Reporter.log("Item: " + item_list.get(z) + " is not a match");			    		
						    	}
					    	}
					    }
				    	Reporter.log("Print item_list:");
				    	for(int i = 0; i < item_list.size(); i++)
				    	{
				    		String temp = item_list.get(i).toString();
				    		Reporter.log(temp);
				    	    printWriter.println("Print items in item_list after confirmation page verification:");
				    	    printWriter.println(item_list.get(i));
				    	}	
				    	*/
				    	count++;
		  	    		File scrFile1 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		  	    		FileUtils.copyFile(scrFile1, new File("C:\\" +"Order" + count +"PlaceOrder_screenshot" +"-" + filename + ".png"));
				    	
				  	    if (isElementPresent("cart-checkout"))
				  	    {
				  	    	driver.findElement(By.id("cart-checkout")).click(); 
				  	    	if (isElementPresent("thankYouMessage"))
				  	    	{
				  	    		Reporter.log("Order Total: " + driver.findElement(By.id("totalDue")).getText());
				  	    		printWriter.println("Order Total: " + driver.findElement(By.id("totalDue")).getText());
				  	    		Reporter.log("Order submitted!");
				  	    		printWriter.println("Order submitted!");
				  	    		Reporter.log("Test case: Pass");
				  	    		printWriter.println("Test case: Pass");
				  	    	}
				  	    	else 
				  	    	{
				  	    		File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				  	    		FileUtils.copyFile(scrFile2, new File("C:\\" +"Order" + count +"screenshot" +"-" + filename + ".png"));
				  	    		Reporter.log("Order not submitted!");
				  	    		printWriter.println("Order not submitted!");
				  	    		Reporter.log("Test case: Fail");
				  	    		printWriter.println("Test case: Fail");
				  	    	}
				  	    }
				        Thread.sleep(10000);
			  		}
			        driver.close();
			        Reporter.log("=============================================================================Order " + count + " Completed================================================================================================");
			        printWriter.println("=============================================================================Order " + count + " Completed================================================================================================");
	  			}
	  			else
	  			{
	  				Reporter.log("Qty is greater than max: " + qtyCount);
	  	    		printWriter.println("Qty is greater than max: "+ qtyCount);
	  			}
		    }
	  		printWriter.close(); 	
	    }
	    public static boolean isElementPresent(String selector) 
		{
	    	boolean present;
	    	if(selector.startsWith("/") )
	    	{
	    		try {
	    			driver.findElement(By.xpath(selector));
	    			present = true;
	    			Reporter.log("Selector found: " + selector);
	    			} 
	    		catch (NoSuchElementException e) {
	    			present = false;
	    			Reporter.log("Selector not found: "+ selector);
	    			}
	    	}
	    	else 
	    	{
	    		try {
		    		driver.findElement(By.id(selector));
		    		present = true;
		    		  Reporter.log("Selector found: " + selector);
		    		  } 
	    		catch (NoSuchElementException e) {
		    		present = false;
		    		Reporter.log("Selector not found: "+ selector);
		    		}
	    	}
	    	return present;
		 }
	    public static String convertToName(String chkoutXpath) 
	   	{
	    	String temp ="";
			if (chkoutXpath == "//*[@id='totals']/div[2]/form/button[1]")
			{
				temp = "Regular Checkout";
			}
			if (chkoutXpath == "//*[@id='totals']/div[2]/form/button[2]")
			{
				temp = "PayPal";
			}
			if (chkoutXpath == "//*[@id='totals']/div[3]/form/div[3]/button")
			{
				temp = "QuickBuy";
			}
			return temp;
	   	}
}
